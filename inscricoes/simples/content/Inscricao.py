# -*- coding: utf-8 -*-

from zope.interface import implements

from Products.Archetypes import atapi
from Products.ATContentTypes.content import base
from Products.ATContentTypes.content import schemata

# -*- Message Factory Imported Here -*-
from inscricoes.simples import simplesMessageFactory as _

from inscricoes.simples.interfaces import IInscricao
from inscricoes.simples.config import PROJECTNAME

from Products.validation import validation
from validators import DuplicadoIdValidator
validation.register(DuplicadoIdValidator('isDuplicadoId'))

InscricaoSchema = schemata.ATContentTypeSchema.copy() + atapi.Schema((

    atapi.StringField(
        name='title',
        storage=atapi.AnnotationStorage(),
        widget=atapi.StringWidget(
            label=_(u"Nome completo"),
            description=_(u""),
        ),
        required=1,
        accessor='Title',
        validators=('isDuplicadoId'),
    ),

    atapi.StringField(
        name='contato',
        storage=atapi.AnnotationStorage(),
        widget=atapi.StringWidget(
            label=_(u"Contato"),
            description=_(u""),
        ),
        required=1,
    ),

    atapi.StringField(
        name='telefone',
        storage=atapi.AnnotationStorage(),
        widget=atapi.StringWidget(
            label=_(u"Telefone"),
            description=_(u"Ex: 0XX 0000 0000"),
        ),
        required=1,
    ),

    atapi.StringField(
        name='email',
        storage=atapi.AnnotationStorage(),
        widget=atapi.StringWidget(
            label=_(u"Email"),
            description=_(u""),
        ),
        required=1,
        validators=('isEmail'),
    ),

    atapi.StringField(
        name='instituicao',
        storage=atapi.AnnotationStorage(),
        widget=atapi.StringWidget(
            label=_(u"Instituição"),
            description=_(u""),
        ),  
        required=1,
    ), 

    atapi.StringField(
        name='vinculo',
        storage=atapi.AnnotationStorage(),
        widget=atapi.SelectionWidget(
            label=_(u"Vinculo"),
            description=_(u""),
        ),
        required=1,
        vocabulary=['Residente','Preceptor/tutor','Coordenador', 'Gestor', 'Docente', 'Outro'],
    ),

    atapi.StringField(
        name='soubeevento',
        storage=atapi.AnnotationStorage(),
        widget=atapi.StringWidget(
            label=_(u"Como soube do evento?"),
            description=_(u""),
        ),  
        required=1,
    ), 

))

# Set storage on fields copied from ATContentTypeSchema, making sure
# they work well with the python bridge properties.

InscricaoSchema['title'].storage = atapi.AnnotationStorage()
InscricaoSchema['description'].storage = atapi.AnnotationStorage()
InscricaoSchema['description'].widget.visible = {'edit': 'invisible', 'view': 'invisible'}

schemata.finalizeATCTSchema(InscricaoSchema, moveDiscussion=False)

for field in ['subject','language','creators','contributors','rights','excludeFromNav','relatedItems',
              'effectiveDate','expirationDate','allowDiscussion','location','description']:
    InscricaoSchema[field].write_permission = "ReviewPortalContent"

class Inscricao(base.ATCTContent):
    """Description of the Example Type"""
    implements(IInscricao)

    meta_type = "Inscricao"
    schema = InscricaoSchema

    title = atapi.ATFieldProperty('title')
    description = atapi.ATFieldProperty('description')

    # -*- Your ATSchema to Python Property Bridges Here ... -*-
    contato = atapi.ATFieldProperty('contato')
    telefone = atapi.ATFieldProperty('telefone')
    email = atapi.ATFieldProperty('email')
    instituicao = atapi.ATFieldProperty('instituicao')
    vinculo = atapi.ATFieldProperty('vinculo')
    soubeevento = atapi.ATFieldProperty('soubeevento')
    
    def Title(self):
        """ Sete Title e email
        """
        self.setEmail(self.get_member_fullname_email()[1])
        return self.get_member_fullname_email()[0]

    def get_member_fullname_email(self):
        """Obtem o nome completo e email
        """
        try:
            portal = self.portal_url.getPortalObject()
            user = portal.portal_membership.getMemberById(self.Creator())
            fullname = user.getProperty('fullname','Fullname missing')
            email = user.getProperty('email',None)
            if not fullname:
                fullname = self.Creator()
        except:
            fullname = ''
            email = ''
        return (fullname, email)


atapi.registerType(Inscricao, PROJECTNAME)
