# -*- coding: utf-8 -*-
from Acquisition import aq_parent
from Acquisition import aq_inner
from Products.CMFCore.utils import getToolByName

from zope.interface import implements
from Products.validation.i18n import PloneMessageFactory as _
from Products.validation.i18n import recursiveTranslate
from Products.validation.i18n import safe_unicode
from Products.validation.interfaces.IValidator import IValidator

class DuplicadoIdValidator:
    implements(IValidator)

    def __init__(self, name, title='', description='', showError=True):
        self.name = name
        self.title = title or name
        self.description = description
        self.showError = showError

    def __call__(self, value, *args, **kwargs):
        instance = kwargs.get('instance', None)
        field    = kwargs.get('field', None)
        
        if instance.isTemporary() is False:
            return True

        request = getattr(instance, 'REQUEST', None)
        parent = request.get('PARENTS')[0]
        parentPath = '/'.join(parent.getPhysicalPath())
        catalog = getToolByName(instance, 'portal_catalog')

        obj_search = catalog(Creator=instance.Creator(),
                             Type = instance.Type(),
                             path={'query':parentPath})

        if len(obj_search) == 0:
            return False
        else:
            return """Você já possui um formulário cadastrado!"""
