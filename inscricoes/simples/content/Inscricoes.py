# -*- coding: utf-8 -*-

from zope.interface import implements

from Products.Archetypes import atapi
from Products.ATContentTypes.content import folder
from Products.ATContentTypes.content import schemata

from Products.CMFCore.utils import getToolByName
from Products.CMFPlone.utils import _createObjectByType

# -*- Message Factory Imported Here -*-
from inscricoes.simples import simplesMessageFactory as _

from inscricoes.simples.interfaces import IInscricoes
from inscricoes.simples.config import PROJECTNAME

InscricoesSchema = folder.ATFolderSchema.copy() + atapi.Schema((

    # -*- Your Archetypes field definitions here ... -*-
    atapi.TextField(
        name='corpotexto',
        allowable_content_types=('text/plain', 'text/structured', 'text/html', 'application/msword',),
        storage=atapi.AnnotationStorage(),
        widget=atapi.RichWidget(
            label=_(u"Corpo do Texto"),
            description=_(u""),
        ),
        default_output_type='text/html',
    ),  

))

# Set storage on fields copied from ATFolderSchema, making sure
# they work well with the python bridge properties.

InscricoesSchema['title'].storage = atapi.AnnotationStorage()
InscricoesSchema['description'].storage = atapi.AnnotationStorage()

schemata.finalizeATCTSchema(
    InscricoesSchema,
    folderish=True,
    moveDiscussion=False
)


class Inscricoes(folder.ATFolder):
    """Description of the Example Type"""
    implements(IInscricoes)

    meta_type = "Inscricoes"
    schema = InscricoesSchema

    title = atapi.ATFieldProperty('title')
    description = atapi.ATFieldProperty('description')

    # -*- Your ATSchema to Python Property Bridges Here ... -*-
    corpotexto = atapi.ATFieldProperty('corpotexto')

    # Methods
    def at_post_create_script(self):
        """Creates additional content
        """

        content = self.objectIds()
        tool = getToolByName(self, 'portal_workflow')
        all_types = getToolByName(self, 'portal_types')

        def workflow(obj, transition='reject'):
            try:
                tool.doActionFor(obj, transition)
            except:
                pass

        if 'arquivos' not in content:
            obj = _createObjectByType('Folder', self,
                                      'arquivos', title='Arquivos', description='')
            workflow(obj,'publish')
            list_types = ['Image', 'File']
            self.arquivos.setConstrainTypesMode(True)
            self.arquivos.setLocallyAllowedTypes(list_types)
            self.arquivos.setImmediatelyAddableTypes(list_types)
            self.arquivos.manage_addProperty('layout','folder_summary_view','string')


atapi.registerType(Inscricoes, PROJECTNAME)
