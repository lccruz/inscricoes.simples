"""Common configuration constants
"""

PROJECTNAME = 'inscricoes.simples'

ADD_PERMISSIONS = {
    # -*- extra stuff goes here -*-
    'Inscricao': 'inscricoes.simples: Add Inscricao',
    'Inscricoes': 'inscricoes.simples: Add Inscricoes',
}
