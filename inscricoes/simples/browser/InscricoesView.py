# -*- coding: utf-8 -*-

from Acquisition import aq_inner
from zope.interface import implements, Interface
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile

from Products.CMFCore.utils import getToolByName

from inscricoes.simples.interfaces import IInscricoes

class InscricoesView(BrowserView):
    """Default view of a Inscricoes folder
    """
    implements(IInscricoes)

    def getCadastro(self):
        """Retorna o cadastro do usuario logado, caso exista
        """
        catalog = getToolByName(self.context, 'portal_catalog')
       
        path = '/'.join(self.context.getPhysicalPath())
        portal_membership = getToolByName(self.context, "portal_membership")
        member_id = portal_membership.getAuthenticatedMember().getId()
        link = []
        if member_id:
            allObj = catalog(path={'query':path},
                             portal_type="Inscricao",
                             Creator=member_id)
            if allObj:
                obj = allObj[0]
                obj = obj.getObject()
                url = obj.absolute_url()
                link.append({"nome":obj.title_or_id(),
                             "url":url})
        return link
