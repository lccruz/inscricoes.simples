# -*- coding: utf-8 -*-
from Acquisition import aq_parent
from Acquisition import aq_inner
from Products.Archetypes.utils import shasattr
from Products.CMFCore.utils import getToolByName

def filterTemporaryItems(obj):
   """Check if the item has an acquisition chain set up and is not
   of temporary nature, i.e. still handled by the `portal_factory`;
   if so return it, else return None."""
   parent = aq_parent(aq_inner(obj))
   if parent is None:
       return None
   if shasattr(obj, 'isTemporary'):
       if obj.isTemporary():
           return None
   return obj

def send_email_after_add_formulario(obj, list_editor_full):
    """Envia email ao cadastrar um novo formulario na pasta"""
    try:
        portal = obj.portal_url.getPortalObject()
        mFrom = portal.MailHost.smtp_uid
        mFrom = mFrom.replace('=','@')
        member = portal.portal_membership.getMemberById(obj.Creator())
        member_fullname = member.getProperty('fullname','Fullname missing')
        member_email = member.getProperty('email',None)

        #send email for editor
        editor_mSubj = "Foi preenchido um novo formulario"
        editor_message = """Mime-Version: 1.0\n"""
        editor_message += """Content-Type: text/plain; charset=UTF-8\n"""
        editor_message += """Titulo formulário:  %s\nCriador formulário: %s\nLink formulário: %s""" % ( 
                        str(obj.Title()),
                        str(member_fullname),
                        obj.absolute_url(),)

        for email in list_editor_full:
            mTo = email
            portal.MailHost.send(editor_message, mTo, mFrom, editor_mSubj)

        #send email for owner
        mSubj = "Formulario preenchido com sucesso"
        message = """Mime-Version: 1.0\n"""
        message += """Content-Type: text/plain; charset=UTF-8\n"""
        message += """Formulário preenchido com sucesso \n Link para visualizacao:  %s\n\n""" % (obj.absolute_url())
        mTo = member_email
        if mTo:
            portal.MailHost.send(message, mTo, mFrom, mSubj)

    except:
        print "Erro ao enviar o email"


def sendMail(obj, event):
    """sendMail"""
    doc = filterTemporaryItems(event.object)
    if doc is None:
        return
    #send mail
    list_editor_full = ['lccruzx@gmail.com',]
    send_email_after_add_formulario(obj,list_editor_full)
